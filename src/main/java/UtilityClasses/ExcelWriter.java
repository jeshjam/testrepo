package UtilityLibrary;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelWriter {
	
	public static void writetoExcel(String status, int rowvalue, int cellvalue) throws IOException{
		System.out.println("writing status to excel");
		ConfigReader cr=new ConfigReader();
		String testcasePath=cr.getTestCasePath();
		File f=new File(testcasePath);
		
		FileInputStream fis=new FileInputStream(f);
		Workbook wb=new XSSFWorkbook(fis);
		
		Sheet sht=wb.getSheet("Registration_Test_Cases");
		sht.getRow(rowvalue).createCell(cellvalue).setCellValue(status);
		FileOutputStream fos=new FileOutputStream(f);
		wb.write(fos);
		wb.close();
		
		
	}

}
