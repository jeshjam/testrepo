package UtilityClasses;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class ScreenShotTaker {
	
	public static void takescrnsht(WebDriver d, String scrnshotname)// Here the method is declared as static which means no object is required to call this method . This method can be called using class name from another class.
	{
		TakesScreenshot ts=(TakesScreenshot)d;//type cast webdriver object into TakesScreenshot reference. TakesScreenshot is an interface therefore we can create its reference variable but cannot instantiate it i.e TakesScreenshot ts; is fine but we cannot TakesScreenshot ts=new TakesScreenshot()
		File source=ts.getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(source, new File("E:\\new workspace 2\\GmailAutomation_Using_POM_Version2.0\\screenshots\\"+scrnshotname+".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	


}
