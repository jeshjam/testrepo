package UtilityClasses;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class verifyAllTheLinksOnWebPage {

	public void linkVerification(String a){
		
		try {
			URL url=new URL(a);
			HttpURLConnection httpcon=(HttpURLConnection)url.openConnection();
			httpcon.setConnectTimeout(4000);
			httpcon.connect();
			if(httpcon.getResponseCode()==200){
				System.out.println(a+"-"+httpcon.getResponseMessage());
			}
			else if(httpcon.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND){
				System.out.println(a+"-"+httpcon.getResponseMessage());
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}
}
