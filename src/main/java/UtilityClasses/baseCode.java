package UtilityClasses;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

public class baseCode {
	
	
	String chromePath;
	String iePath;
	String firefoxPath;
	protected WebDriver driver;
	
	@BeforeTest
	@Parameters({"browser","environment"})
	public void setup(@Optional("chrome")String browser, @Optional("remote")String environment) throws MalformedURLException{
		if(browser.equalsIgnoreCase("chrome") && environment.equalsIgnoreCase("remote")){
			//optional means agar if it doesn't find any other option then it will run chrome bydefault.
			
			DesiredCapabilities dc= DesiredCapabilities.chrome();
			
			dc.setBrowserName("chrome");
			dc.setVersion("76.0.3809.100");
			dc.setPlatform(Platform.WINDOWS);
			
			//dc.setCapability("os", "Windows 10");
			
			
			URL node=new URL("http://192.168.1.7:5555/wd/hub");//remember the port number here should be of node i.e. 5555
			
			
			
			driver=new RemoteWebDriver(node,dc);
			
			//driver=new RemoteWebDriver(hub,dc);//passing hub in remote driver, this hub will decide which node to execute according to above desired capabilities
			//also you can directly pass node in remotewebdriver.
			
			driver.get("http://www.flipkart.com");
		}
		else if(browser.equalsIgnoreCase("firefox") && environment.equalsIgnoreCase("remote")){
			DesiredCapabilities dc= DesiredCapabilities.firefox();
			dc.setBrowserName("Firefox");
			dc.setVersion("47.0.2");
			dc.setPlatform(Platform.WINDOWS);
		}
		else if(browser.equalsIgnoreCase("ie") && environment.equalsIgnoreCase("remote")){
			DesiredCapabilities dc= DesiredCapabilities.internetExplorer();
			dc.setBrowserName("Internet Explorer");
			dc.setVersion("11.765.17134.0");
			dc.setPlatform(Platform.WINDOWS);
URL node=new URL("http://192.168.1.7:5566/wd/hub");//remember the port number here should be of node i.e. 5555
			
			driver=new RemoteWebDriver(node,dc);
			
			//driver=new RemoteWebDriver(hub,dc);//passing hub in remote driver, this hub will decide which node to execute according to above desired capabilities
			//also you can directly pass node in remotewebdriver.
			
			driver.get("http://www.flipkart.com");
		}
		
		
		else if(browser.equalsIgnoreCase("chrome") && environment.equalsIgnoreCase("grid")){
			//optional means agar if it doesn't find any other option then it will run chrome bydefault.
			System.out.println("**EXECUTING GRID****");
			DesiredCapabilities dc= DesiredCapabilities.chrome();
			
			dc.setBrowserName("chrome");
			
			//dc.setVersion("80.0.3987.149 (Official Build)(64-bit)");
			//dc.setVersion("80.0.3987.149");
			dc.setVersion("80.0.3987.163");
			//dc.setPlatform(Platform.WINDOWS);
			dc.setPlatform(org.openqa.selenium.Platform.WINDOWS);
			//dc.setCapability("os", "Windows 10");
			
			/* ip and port of hub */
			URL node=new URL("http://192.168.1.10:4444/wd/hub");
			
			/* ip and port of node   */
			//URL node=new URL("http://192.168.1.8:33006/wd/hub");
			
			/* ip of node and port of hub       */
			//URL node=new URL("http://192.168.1.8:4444/wd/hub");
			
			driver=new RemoteWebDriver(node,dc);
			
			//driver=new RemoteWebDriver(hub,dc);//passing hub in remote driver, this hub will decide which node to execute according to above desired capabilities
			//also you can directly pass node in remotewebdriver.
			
			driver.get("http://www.flipkart.com");
		}
/***************CLOUD TESTING BROWSER STACK*****************************/		
		else if(browser.equalsIgnoreCase("chrome") && environment.equalsIgnoreCase("cloud")){
			 String USERNAME = "jayeshjamindar1";
			 String AUTOMATE_KEY = "x4sGhQo2je2y8bZrF6G5";
			 String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
			//DesiredCapabilities dc= DesiredCapabilities.chrome();
			//dc.setBrowserName("chrome");
			//dc.setVersion("11.765.17134.0");
			//dc.setPlatform(Platform.WINDOWS);
			 DesiredCapabilities caps = new DesiredCapabilities();
			    caps.setCapability("browser", "Chrome");
			    caps.setCapability("browser_version", "62.0");
			    caps.setCapability("os", "Windows");
			    caps.setCapability("os_version", "10");
			    caps.setCapability("resolution", "1024x768");
			    caps.setCapability("name", "Bstack-[Java] Sample Test");

URL node=new URL(URL);
			
			driver=new RemoteWebDriver(node,caps);
			
			//driver=new RemoteWebDriver(hub,dc);//passing hub in remote driver, this hub will decide which node to execute according to above desired capabilities
			//also you can directly pass node in remotewebdriver.
			
			driver.get("http://www.flipkart.com");
		}
		
		
		
		
		
		else if(browser.equalsIgnoreCase("firefox") && environment.equalsIgnoreCase("cloud")){
			 String USERNAME = "jayeshjamindar1";
			 String AUTOMATE_KEY = "x4sGhQo2je2y8bZrF6G5";
			 String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
			//DesiredCapabilities dc= DesiredCapabilities.chrome();
			//dc.setBrowserName("chrome");
			//dc.setVersion("11.765.17134.0");
			//dc.setPlatform(Platform.WINDOWS);
			 DesiredCapabilities caps = new DesiredCapabilities();
			 caps.setCapability("browser", "Firefox");
			    caps.setCapability("browser_version", "70.0 beta");
			    caps.setCapability("os", "Windows");
			    caps.setCapability("os_version", "10");
			    caps.setCapability("resolution", "1024x768");
			    caps.setCapability("name", "Bstack-[Java] Sample Test");


URL node=new URL(URL);
			
			driver=new RemoteWebDriver(node,caps);
			
			//driver=new RemoteWebDriver(hub,dc);//passing hub in remote driver, this hub will decide which node to execute according to above desired capabilities
			//also you can directly pass node in remotewebdriver.
			
			driver.get("http://www.flipkart.com");
		}
		
		
		
		else if(browser.equalsIgnoreCase("MobileGalaxyS8") && environment.equalsIgnoreCase("cloud")){
			 String USERNAME = "jayeshjamindar1";
			 String AUTOMATE_KEY = "x4sGhQo2je2y8bZrF6G5";
			 String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
			//DesiredCapabilities dc= DesiredCapabilities.chrome();
			//dc.setBrowserName("chrome");
			//dc.setVersion("11.765.17134.0");
			//dc.setPlatform(Platform.WINDOWS);
			 DesiredCapabilities caps = new DesiredCapabilities();
			 caps.setCapability("browserName", "android");
			    caps.setCapability("device", "Samsung Galaxy S8");
			    caps.setCapability("realMobile", "true");
			    caps.setCapability("os_version", "7.0");
			    caps.setCapability("name", "Bstack-[Java] Sample Test");


URL node=new URL(URL);
			
			driver=new RemoteWebDriver(node,caps);
			
			//driver=new RemoteWebDriver(hub,dc);//passing hub in remote driver, this hub will decide which node to execute according to above desired capabilities
			//also you can directly pass node in remotewebdriver.
			
			driver.get("http://www.flipkart.com");
		}
/*************************************END*****************************/
		
		else if(browser.equalsIgnoreCase("chrome") && environment.equalsIgnoreCase("local")){
			//System.setProperty("webdriver.chrome.driver", "F:\\flipkartNEW1sept2019\\src\\main\\resources\\chromedriver.exe");
			System.out.println("*************BEFORE TEST**********\n *******SETTING SYSTEM PROPERTIES");
			//System.setProperty("webdriver.chrome.driver", "../flipkartNEW1sept2019/src/main/resources/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", "../testrepo/src/main/resources/chromedriver.exe");
			
			driver=new ChromeDriver();
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.get("http://www.flipkart.com");
		}
		
		else if(browser.equalsIgnoreCase("firefox") && environment.equalsIgnoreCase("local")){
			//System.setProperty("webdriver.chrome.driver", "F:\\flipkartNEW1sept2019\\src\\main\\resources\\chromedriver.exe");
			System.out.println("*************BEFORE TEST**********\n *******SETTING SYSTEM PROPERTIES");
			//System.setProperty("webdriver.chrome.driver", "../flipkartNEW1sept2019/src/main/resources/chromedriver.exe");
			System.setProperty("webdriver.firefox.driver", "../testrepo/src/main/resources/geckodriver.exe");
			
			driver=new FirefoxDriver();
			driver.get("http://www.flipkart.com");
		}
		else if(browser.equalsIgnoreCase("ie") && environment.equalsIgnoreCase("local")){
			//System.setProperty("webdriver.chrome.driver", "F:\\flipkartNEW1sept2019\\src\\main\\resources\\chromedriver.exe");
			System.out.println("*************BEFORE TEST**********\n *******SETTING SYSTEM PROPERTIES");
			//System.setProperty("webdriver.chrome.driver", "../flipkartNEW1sept2019/src/main/resources/chromedriver.exe");
			System.setProperty("webdriver.ie.driver", "../testrepo/src/main/resources/IEDriverServer.exe");
			
			driver=new InternetExplorerDriver();
			driver.get("http://www.flipkart.com");
		}
	}
	
	@AfterTest //this will get executed after all the @test methods/test cases gets executed in the class i.e. jab kisi test class k sare test cases execute ho jayenge tab ye method execute hogi
	public void end(){
System.out.println("*****executing @AfterTest annotation in base class to close browser*********");

		//driver.close();
driver.quit();
		
	}

}


/*
 * command for creating HUB
 * 
 * java -jar selenium-server-standalone-<version>.jar -role hub
 * 
 * default port will be 4444
 * */


/*
 * command for creating the node:
 * 
 * java -Dwebdriver.chrome.driver=E:\chromepath\chromedriver.exe  -jar selenium-server-standalone-3.141.59.jar -role node -hub http://192.168.1.7:4444/grid/register -port 5555
 * 
 * here we are registering the node to the hub.
 * where 192.168.1.7 is the ip address of the machine where hub is running, 
 * 
 * 
 * 
 * java -jar selenium-server-standalone-<version>.jar -role node -hub https://localhost:4444/grid/register
 *
 * 
 * 
 * 
 * registring node:

java -jar selenium-server-standalone-3.141.59.jar -role node -hub https://localhost:4444/grid/register


***********************************

java -Dwebdriver.chrome.driver=D:\selenium\chromedriver.exe -jar selenium-server-standalone-3.141.59.jar -role node -hub http://192.168.1.10:4444/grid/register

************************************
java -Dwebdriver.chrome.driver=D:\selenium\chromedriver.exe -jar selenium-server-standalone-3.141.59.jar -role node -hub http://192.168.1.10:4444/grid/register -browser "browserName=chrome,version=80.0.3987.149, maxInstances=10, platform=WINDOWS"

 * */
 