package UtilityClasses;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

public class ExcelReader {
	
@DataProvider (name="readingLoginData")
	public static String [][] readRegdata()throws Exception{
	System.out.println("****Inside data provider....");
	configReader cr=new configReader("config");
	String excelPATH=cr.getExcelPath();
		//File f=new File("E:\\Technical 03-april2017\\Test Data second.xlsx");
	    File f=new File(excelPATH);
		FileInputStream fis=new FileInputStream(f);
		Workbook wb=new XSSFWorkbook(fis);
		
		Sheet sht=wb.getSheet("Login");
		int rowcount=sht.getLastRowNum()-sht.getFirstRowNum();
		System.out.println("Last row num="+sht.getLastRowNum()+"\n First row num="+sht.getFirstRowNum()+"\n Total row count"+rowcount);
		int numberOfColumns=sht.getRow(0).getPhysicalNumberOfCells();
		int numOfCol=sht.getRow(0).getLastCellNum();
		String loginData[][]=new String[rowcount][numOfCol];
		for(int i=0;i<rowcount;i++){
			Row r=sht.getRow(i);
			
			for(int j=0;j<numOfCol;j++){
				Cell cell=r.getCell(j);
				Object obj;
				
				//if(cell.getCellType()==0)//this is deprecated in new version of apache poi
				if(cell.getCellType()==CellType.NUMERIC)
				{
					obj=cell.getNumericCellValue();
					
					loginData[i][j]=obj.toString();
					//System.out.println("numeric value="+obj.toString());
				}
				//else if(cell.getCellType()==1)//this is deprecated in new version of apache poi
				else if(cell.getCellType()==CellType.STRING)
				{
					String cellval=cell.getStringCellValue();
					loginData[i][j]=cellval;
					//System.out.println("string value="+cellval);
					
				}
				
				
							
				
				
			}
		}
		return loginData;
	}

@DataProvider (name="readingLoginData2")
public static String [][] readExcel3() throws Exception
{
	System.out.println("in dataprovider,,....");
	File f=new File("E:\\Technical 03-april2017\\Test Data second.xlsx");
	FileInputStream fis=new FileInputStream(f);
	Workbook wb=new XSSFWorkbook(fis);
	
	Sheet sht=wb.getSheet("Login");
	int rowcount=sht.getLastRowNum()-sht.getFirstRowNum();
	int numberOfColumns=sht.getRow(0).getPhysicalNumberOfCells();
	int numOfCol=sht.getRow(0).getLastCellNum();
	String data[][]=new String[rowcount][numOfCol];
	for(int i=0;i<rowcount;i++){
		Row r=sht.getRow(i);
		for(int j=0;j<numOfCol;j++){
			Cell cell=r.getCell(j);
			String cellval=cell.getStringCellValue();
			data[i][j]=cellval;
			System.out.println("value is= "+cellval);
			
		}
	}
return data;	
}
	
}
