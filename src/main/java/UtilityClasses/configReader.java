package UtilityClasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class configReader {

	Properties p;
	File f;
	FileInputStream fis;
	public configReader(String properyFileName) throws IOException{
		//f=new File("../flipkartNEW1sept2019/src/main/resources/"+properyFileName+".property");
		f=new File("../testrepo/src/main/resources/"+properyFileName+".property");
		fis=new FileInputStream(f);
		p=new Properties();
		p.load(fis);
	}
	
	public String giveAllHeaderCategories(){
		String mainCategories=p.getProperty("allHeaderCategories");
		return mainCategories;
	}
	
	public String giveAllValuesInDrodown(){
		String ddValues=p.getProperty("allDropdownValues");
		return ddValues;
	}
	
	
	public String giveLoginLink(){
		String loginLink=p.getProperty("loginLink");
		return loginLink;
	}
	
	public String errMssgonInvalidLogin(){
		String errMsg=p.getProperty("errorMessageOnInvalidLogin");
		return errMsg;
	}
	
	public String cancelLoginWin(){
		String cancel=p.getProperty("cancelLoginWindow");
		return cancel;
	}
	
	public String moreDD(){
		String more=p.getProperty("moreDropdown");
		return more;
	}
	
	public String addToCart(){
		String addtoCart=p.getProperty("addtocart");
		return addtoCart;
	}
	
	public String buynow(){
		String buynowButt=p.getProperty("buynow");
		return buynowButt;
	}
	
	public String allProductsOnSubMenu(){
		String allproducts=p.getProperty("allProducts");
		return allproducts;
	}
	
	public String getExcelPath(){
		String excelpath=p.getProperty("excelPathTestData");
		return excelpath;
	}
	
	public String getLoginEmail(){
		String loginemail=p.getProperty("loginEmail");
		return loginemail;
	}
	
	public String getLoginPassword(){
		String password=p.getProperty("loginpassword");
		return password;
	}
	
	public String getLoginSubmitButton(){
		String buttonSubmit=p.getProperty("loginSubmitButton");
		return buttonSubmit;
	}
	
	public String getMyAccountLink(){
		String My_Acc=p.getProperty("MyAccount");
		return My_Acc;
	}
}
