package Pages;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import UtilityClasses.configReader;

public class homePage {

	By mainCategories;
	By allValues;
	By login;
	By loginEmailField;
	By loginPasswordField;
	By submitButton;
	By errmsgForInvalidLogin;
	By cancelLoginwindow;
	By moredropDown;
	By MyAccount;
	WebDriver d;
	configReader c;
	List<WebElement>categories;
	//WebElement e;
	Actions action;
	
	
	public homePage(WebDriver dr){
		this.d=dr;
		try {
			c=new configReader("object_repository");
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		mainCategories=By.xpath(c.giveAllHeaderCategories());
		allValues=By.xpath(c.giveAllValuesInDrodown());
		login=By.xpath(c.giveLoginLink());
		errmsgForInvalidLogin=By.xpath(c.errMssgonInvalidLogin());
		cancelLoginwindow=By.xpath(c.cancelLoginWin());
		moredropDown=By.xpath(c.moreDD());
		loginEmailField=By.xpath(c.getLoginEmail());
		loginPasswordField=By.xpath(c.getLoginPassword());
		submitButton=By.xpath(c.getLoginSubmitButton());
		MyAccount=By.xpath(c.getMyAccountLink());
	}
	
	
	public void findingAllMainCategories(){
		d.manage().window().maximize();
		WebElement e;
		categories=d.findElements(mainCategories);
		for(int i=0;i<categories.size();i++){
			e=categories.get(i);//this will return webelement at ith index
			System.out.println("Main categories are--->"+e.getText());
		}
	}
	
	public void mouseHoveringOnMainCategories(){
		WebElement e=null;
		List<WebElement>elements=d.findElements(mainCategories);
		action=new Actions(d);
		WebDriverWait wait=new WebDriverWait(d,10);
		
		if(d.findElement(cancelLoginwindow).isDisplayed()){
			d.findElement(cancelLoginwindow).click();
		}
		for(int i=0;i<elements.size();i++){
			e=elements.get(i);
			//d.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
			wait.until(ExpectedConditions.visibilityOf(e));
			action.moveToElement(e).build().perform();
			
		}
		//action.moveToElement().build().perform();
	}
	
		public void findingAllValuesUnderMainCategories(){
		
		WebElement e=null;
		WebElement ele=null;
		List<WebElement>elements=d.findElements(mainCategories);
		action=new Actions(d);
		
		if(d.findElement(cancelLoginwindow).isDisplayed()){
			d.findElement(cancelLoginwindow).click();
		}
		for(int i=0;i<elements.size();i++){
			e=elements.get(i);
			action.moveToElement(e).build().perform();
			d.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			List<WebElement>ddvalues=d.findElements(allValues);
			d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			for(int y=0;y<ddvalues.size();y++){
				ele=ddvalues.get(y);
				System.out.println("Values under main categories are--->"+ele.getText());	
			}
			
		}
		
		
	}
		
		public subMenuPage clickingOnProduct() throws InterruptedException{
		
			
			WebDriverWait wait=new WebDriverWait(d,10);
			
			if(d.findElement(cancelLoginwindow).isDisplayed()){
				System.out.println("!!!!!!!---LOGIN WINDOW visible BEFORE CLICKING PRODUCT.. CLOSING IT");
				d.findElement(cancelLoginwindow).click();
			}
			List<WebElement> allcategories=d.findElements(mainCategories);
			
			/*System.out.println("<--CATEGORIES ARE-->");
			for(int y=0;y<allcategories.size();y++){
				System.out.println(allcategories.get(y).getText());
			}*/
			
			allcategories.get(0).click();
			
			//d.manage().timeouts().implicitlyWait(9, TimeUnit.SECONDS);
			Thread.sleep(2000);
			List<WebElement>dropDownValues=d.findElements(allValues);
			System.out.println("TOTAL NUMBER OF DROPDOWN ITEMS ARE->"+dropDownValues.size());
			
			
			//ddvalues.get(3).sendKeys(Keys.ENTER);
			
			/*for(int r=0;r<dropDownValues.size();r++){
				String attribute=dropDownValues.get(r).getAttribute("title");
				String inner_html=dropDownValues.get(r).getAttribute("innerHTML");
				System.out.println("values="+dropDownValues.get(r).getText());
				System.out.println("Attribute="+attribute+"\n innerhtml="+inner_html);
			}*/
			
			
			//wait.until(ExpectedConditions.visibilityOf(dropDownValues.get(6)));
			Thread.sleep(2000);
			dropDownValues.get(6).click();
			Thread.sleep(5000);
					
			
			
			/*for(int i=14;i<dropDownValues.size();i++){
				//wait.until(ExpectedConditions.visibilityOf(dropDownValues.get(i)));
				//dropDownValues.get(i).getAttribute("");
					dropDownValues.get(i).click();
					
					System.out.println("TRIED CLICKING ELEMENT---------->>>"+dropDownValues.get(i).getText());
				}*/
				//System.out.println("TRYING TO CLICK---------->>>"+dropDownValues.get(3).getText());
				/*Actions act=new Actions(d);
				act.moveToElement(dropDownValues.get(3)).build().perform();
				act.click(dropDownValues.get(3)).build().perform();*/
				/*act.click(dropDownValues.get(3));
				Action act2=act.build();
				act2.perform();*/
				
				/*JavascriptExecutor jse=(JavascriptExecutor)d;
				jse.executeScript("arguments[0].click();", dropDownValues.get(3));*/
							
			
			return new subMenuPage(d);
		}
		
		
		
		
		public void clickLoginLink(){
			d.findElement(login).click();
		}
		
		
		
		public void clickCancelLogin(){
			d.findElement(cancelLoginwindow).click();
		}
		
		
		public String enterValidLoginCred(String email, String pass){
			System.out.println("*******ENTERING VALID CREDENTIALS****************");
			if(d.findElement(cancelLoginwindow).isDisplayed()){
				System.out.println("LOGIN WINDOW visible.. CLOSING IT");
				d.findElement(cancelLoginwindow).click();
			}
			
			d.findElement(login).click();
			
			String current=d.getWindowHandle();
			Set<String> allHandles=d.getWindowHandles();
			
			for(String p:allHandles){
				if(!current.equalsIgnoreCase(p)){
					d.switchTo().window(p);
				}
			}
			
			d.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			d.findElement(loginEmailField).sendKeys(email);
			d.findElement(loginPasswordField).sendKeys(pass);
			d.findElement(submitButton).click();
			return d.findElement(MyAccount).getText();
			//return MyAccount.toString();
		}
		
		public String enterInvalidLoginCred(String email, String pass){
			System.out.println("*******ENTERING INVALID CRED****************");
			//d.findElement(login).click();
			//d.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			/*String loginwindowHandle=d.getWindowHandle();
			d.switchTo().window(loginwindowHandle);*/
			
			if(d.findElement(cancelLoginwindow).isDisplayed()){
				System.out.println("LOGIN WINDOW visible.. CLOSING IT");
				d.findElement(cancelLoginwindow).click();
			}
			
			d.findElement(login).click();
			String current=d.getWindowHandle();
			Set<String> allHandles=d.getWindowHandles();
			
			for(String p:allHandles){
				if(!current.equalsIgnoreCase(p)){
					d.switchTo().window(p);
				}
			}
			d.findElement(loginEmailField).sendKeys(email);
			System.out.println("GOING to fill password%%%%%%%%%%%%%%");
			d.findElement(loginPasswordField).sendKeys(pass);
			d.findElement(submitButton).click();
			return d.findElement(errmsgForInvalidLogin).getText();
		}
}
