package Pages;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import UtilityClasses.configReader;

public class subMenuPage {
	
	WebDriver submenuDriver;
	By listOfProducts;
	configReader c;
	
	public subMenuPage(WebDriver dri){
		this.submenuDriver=dri;
		try {
			c=new configReader("object_repository");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		listOfProducts=By.xpath(c.allProductsOnSubMenu());
		System.out.println(listOfProducts.toString());
	}
	
	
	public productDetailPage clickOnProduct(){
		
		List<WebElement>products=submenuDriver.findElements(listOfProducts);
		System.out.println("No. Of Products are"+products.size());
		for(int i=0;i<products.size();i++){
			System.out.println("Products are here->"+products.get(i).getText());
		}
		products.get(1).click();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		return new productDetailPage(submenuDriver);
	}

}
