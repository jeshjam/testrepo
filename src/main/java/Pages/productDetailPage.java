package Pages;

import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import UtilityClasses.configReader;

public class productDetailPage {

	By addToCart;
	By buyNow;
	WebDriver productpageDriver;
	
	configReader cr;
	public productDetailPage(WebDriver dri){
		this.productpageDriver=dri;
		try {
			cr=new configReader("object_repository");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		addToCart=By.xpath(cr.addToCart());
		buyNow=By.xpath(cr.buynow());
		
	}
	
	public cartPage addproductToCart(){
		String submenuHandle=productpageDriver.getWindowHandle();
		Set<String>handles=productpageDriver.getWindowHandles();
		for(String p:handles){//enhanced for loop, it will automatically iterate the values one by one. handles keep returning the values one by one in each iteration
			if(!submenuHandle.equalsIgnoreCase(p)){
				System.out.println("++++SWITCHING TO ANOTHER WINDOW ---------->");
				productpageDriver.switchTo().window(p);
			}
		}
		
		productpageDriver.findElement(addToCart).click();
		return new cartPage(productpageDriver);
	}
}
