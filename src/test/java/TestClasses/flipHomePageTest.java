package TestClasses;


import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.homePage;
import UtilityClasses.ExcelReader;
import UtilityClasses.baseCode;
//import junit.framework.Assert;




public class flipHomePageTest extends baseCode{
	
	
	//homePage hp;
	
	@BeforeMethod //this will be executed before every @test method/ test case in this class i.e. this will get executed before every test case execution i.e. har test case execution ke pehle ye  method call hogi
	public void executingHomePageTests(){
		System.out.println("*************BEFORE METHOD**********\n EXECUTING HOME PAGE TESTS*******");
		//hp=new homePage(driver);
		
	}

	/*@Test(priority=0)
	public void checkAllMainCategories(){
	
		System.out.println("RUNNING Homepage Test");
		homePage hp=new homePage(driver);
		hp.findingAllMainCategories();
		//driver.close();
		
	}
	
	@Test(priority=2)
	public void checkMouseHoverOnCategories(){
		homePage hp=new homePage(driver);
		hp.mouseHoveringOnMainCategories();
	}
	
	@Test(priority=1)
	public void checkAllddValuesunderMainCategories(){
		homePage hp=new homePage(driver);
		hp.findingAllValuesUnderMainCategories();
	}
	*/
	
	
	/*ye method excel mei jitni rows hai utni bar call hoga ek k baad ek*/
	@Test(dataProvider="readingLoginData", dataProviderClass=ExcelReader.class, priority=0)
	public void testInvalidLogin(String tc_id, String description, String email, String pass){
		
		String errorMsg=null;
		homePage hp=new homePage(driver);
		//hp=new homePage(driver);
		//hp.clickLoginLink();
		
		if(description.equals("invalid credentials")){
			
			System.out.println("TEST WITH INVALID CREDENTIALS,\n -> Passing "+email+"AND "+pass);
		errorMsg=hp.enterInvalidLoginCred(email, pass);
		Assert.assertEquals("Please enter valid Email ID/Mobile number", errorMsg);
		}
		/*else if(description.equals("valid credentials")){
			
			System.out.println("TEST WITH VALID CREDENTIALS, \n Passing "+email+"AND "+pass);
			String myacc=hp.enterValidLoginCred(email, pass);
			Assert.assertEquals("My Account", myacc);
		}*/
		
	}
	
	
	
	@Test(dataProvider="readingLoginData", dataProviderClass=ExcelReader.class, priority=1)
	public void testValidLogin(String tc_id, String description, String email, String pass){
		
		//String errorMsg=null;
		homePage hp=new homePage(driver);
		//hp=new homePage(driver);
		//hp.clickLoginLink();
		
if(description.equals("valid credentials")){
			
			System.out.println("TEST WITH VALID CREDENTIALS, \n Passing "+email+"AND "+pass);
			String myacc=hp.enterValidLoginCred(email, pass);
			Assert.assertEquals("My Account", myacc);
					
}
		
	}
	
	
	
	@Test(priority=3)
	public void clicksubmenu(){
		System.out.println("<<<<<<<<---Test clicksubmenu-----------");
		homePage hp=new homePage(driver);
		try {
			hp.clickingOnProduct();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(driver.getCurrentUrl());
		Assert.assertEquals("https://www.flipkart.com/mobiles/~pixel-3a-series/pr?sid=tyy%2C4io&otracker=nmenu_sub_Electronics_0_Pixel%203a%20%7C%203a%20XL", driver.getCurrentUrl());
	}
	
	@AfterMethod //this will be executed after every @test method/ test case in this class i.e. this will get executed after every test case execution i.e. har test case execution ke baad ye  method call hogi 
	public void endingHomepageTests(){
		System.out.println("*************AFTER METHOD**********\n ENDING HOME PAGE TESTS*******");
		//hp=null;
		//driver.close();
	//driver.quit();
	}
}
