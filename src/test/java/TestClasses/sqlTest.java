package TestClasses;
/*import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.SQLException;
*/

import java.sql.*;

import org.testng.annotations.Test;

public class sqlTest {

	//Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	Connection conn = null;
	
	String DBURL="jdbc:sqlserver://localhost:1433;databaseName=JayeshDatabase;integratedSecurity=true";
	@Test
	void testSQL(){
		try{
			conn=DriverManager.getConnection(DBURL);
			if (conn != null) {
                DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
                System.out.println("Driver name: " + dm.getDriverName());
                System.out.println("Driver version: " + dm.getDriverVersion());
                System.out.println("Product name: " + dm.getDatabaseProductName());
                System.out.println("Product version: " + dm.getDatabaseProductVersion());
            }
		}
		catch(SQLException ex){
			ex.printStackTrace();
		}
	}
}

/*NOTE:SQL Server has two authentication modes:
Windows authentication: using current Windows user account to log on SQL Server. This mode is for the case both the client and the SQL server are running on the same machine. We specify this mode by adding the property integratedSecurity=true to the URL.
SQL Server authentication: using a SQL Server account to authenticate. We have to specify username and password explicitly for this mode.
Following are some examples:
-          Connect to default instance of SQL server running on the same machine as the JDBC client, using Windows authentication:

jdbc:sqlserver://localhost;integratedSecurity=true;

-          Connect to an instance named sqlexpress on the host dbServer, using SQL Server authentication:

jdbc:sqlserver://dbHost\sqlexpress;user=sa;password=secret

-          Connect to a named database testdb on localhost using Windows authentication:

jdbc:sqlserver://localhost:1433;databaseName=testdb;integratedSecurity=true;

*/