package TestClasses;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.homePage;
import Pages.subMenuPage;
import UtilityClasses.baseCode;

public class subMenuPageTest extends baseCode{

	subMenuPage smp;
	
	@BeforeMethod
	public void prerequisiteForsubMenuPageTest() throws InterruptedException{
		System.out.println("******EXECUTING PreConditions before sub-menu page test **********");
		homePage hp=new homePage(driver);
		smp=hp.clickingOnProduct();
		Thread.sleep(2000);
	}
	
	@Test(priority=0)
	public void checkProductClick(){
	
		System.out.println("Test checkproductclick------------------");
		smp.clickOnProduct();
	}
	
	@AfterMethod
	void closingTest(){
		System.out.println("*************AFTER METHOD**********\n ENDING SUB-MENU PAGE TESTS*******");
		driver.close();
	}
}
