package TestClasses;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class apiLatestTest {
	
	@Test
		void getWeatherDetails(){
		
		//specify base URI
		RestAssured.baseURI="http://restapi.demoqa.com/utilities/weather/city";
		
		//request object
		RequestSpecification requestObject=RestAssured.given();//with requestObject object we will send request to the server
		
		
		//the response received will be stored in responseObject
		Response responseObject=requestObject.request(Method.GET, "/Hyderabad");
		
		//getBody() will return response in json form , therefore we use asString() will convert json form in string 
		String str=responseObject.getBody().asString();
		
		System.out.println("Response body is:"+str);
		
		int actualStatusCode=responseObject.getStatusCode();
		
		//200 is the expected, while actialStatusCode is actual
		Assert.assertEquals(200,actualStatusCode);
	}

}
