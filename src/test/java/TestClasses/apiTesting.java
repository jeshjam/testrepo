package TestClasses;

import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class apiTesting {

	static Response resp, resp2, resp4;
	public static void main (String args[]){
		//resp= RestAssured.get("https://stage.sedexonline.com/sso/oauth/authorize");
/* -------------------------Getting response ---------------------- */		
		resp= RestAssured.get("http://dummy.restapiexample.com/api/v1/employees");
		//String str=resp.getContentType();
		System.out.println("As STRING=========>>>\n"+resp.asString());
		
		JsonPath j=resp.body().jsonPath();
		//System.out.println(j.toString());
		
			
		ResponseBody rb=resp.body();
		System.out.println(rb.asString());
		
		resp2=RestAssured.get("http://dummy.restapiexample.com/api/v1/employee/66686");
		System.out.println(resp2.asString());
		

		
/*-----------Posting something to the server/DB---------------------------*/		
		//RestAssured.post("http://dummy.restapiexample.com/api/v1/create");
		RestAssured.baseURI="http://dummy.restapiexample.com/api/v1";
		RequestSpecification rs=RestAssured.given();
		JSONObject js=new JSONObject();
		js.put("id", "67033");
		js.put("employee_name", "Mahesh");
		js.put("employee_salary", "90800");
		js.put("employee_age", "75");
		//js.put("profile_image", "");
		
		// Add the Json to the body of the request
		rs.header("Content-Type","application/json");
		rs.body(js.toJSONString());
		
		
		// Post the request
		Response resp3=rs.post("/create");
		System.out.println(resp3.getStatusCode());
		resp4=RestAssured.get("http://dummy.restapiexample.com/api/v1/employee/67033");
		System.out.println(resp4.asString());
		
		
		
	}
}
