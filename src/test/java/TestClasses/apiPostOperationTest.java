package TestClasses;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class apiPostOperationTest {

	@Test
	void postOperation(){
		//specify base URI
				RestAssured.baseURI="http://restapi.demoqa.com/customer";
				
				//request object
				RequestSpecification requestObject=RestAssured.given();//with requestObject object we will send request to the server
				
				JSONObject paramWhichAreToBePostedOnServer =new JSONObject();
				
				paramWhichAreToBePostedOnServer.put("FirstName", "jai");
				paramWhichAreToBePostedOnServer.put("LastName", "jam");
				paramWhichAreToBePostedOnServer.put("UserName", "jeshjam");
				paramWhichAreToBePostedOnServer.put("Password", "xyz123");
				paramWhichAreToBePostedOnServer.put("Email", "jaijam@gmail.com");
				
				//specify what kind of data we r sending to server, here it is json data
				requestObject.header("Content-Type","application/json");
				
				//here all the params are converted into json and attached to body
				requestObject.body(paramWhichAreToBePostedOnServer.toJSONString());
				
				Response responseObject=requestObject.request(Method.POST, "/register");
				
				
				//getBody() will return response in json form , therefore we use asString() will convert json form in string 
				String str=responseObject.getBody().asString();
				
				System.out.println("Response body is:"+str);
				
				int actualStatusCode=responseObject.getStatusCode();
				
				//201 is the expected, while actialStatusCode is actual
				Assert.assertEquals(201,actualStatusCode);
	}
}
