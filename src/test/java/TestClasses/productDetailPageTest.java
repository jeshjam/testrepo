package TestClasses;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.homePage;
import Pages.productDetailPage;
import Pages.subMenuPage;
import UtilityClasses.baseCode;

public class productDetailPageTest extends baseCode{

	productDetailPage pdp;
	subMenuPage smp;
	
	@BeforeMethod
	public void preRequisiteForProductDetailPageTest() throws InterruptedException{
		System.out.println("*******EXECUTING PreConditions before product details page test ****---------");
		homePage hp=new homePage(driver);
		smp=hp.clickingOnProduct();
		pdp=smp.clickOnProduct();
	}
	
	@Test
	public void testAddToCart(){
		System.out.println("Test add to cart ------------------");
		pdp.addproductToCart();
	}
	
	
	
	@AfterMethod
	void closingTest(){
		System.out.println("*************AFTER METHOD**********\n ENDING PRODUCT DETAIL  PAGE TESTS*******");
		driver.close();
	}
}
